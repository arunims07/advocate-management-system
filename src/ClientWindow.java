import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClientWindow extends JFrame implements ActionListener {


    JLabel label1,label3,ltitle,copyRight;
    Icon icon1;
    JButton newClientInfo;
    JButton exitClientInfo;
    JButton updateClientInfo;
    JButton miscButton;
    JPanel panel;

    public ClientWindow()
    {
        //--------------------------------------------------------------------------------------------------------------------------------------//
        super ("AMS - User Window");
        setIconImage (getToolkit().getImage ("images/IconLogo.png"));
        Container c=getContentPane();
        Font f1=new Font("Algerian",Font.BOLD,20);
        Font f3=new Font("Arial",Font.ITALIC,12);
        c.setLayout(null);
        ltitle=new JLabel("Client Information");
        c.add(ltitle);
        ltitle.setBounds(150,5,400,75);
        ltitle.setFont(f1);
        ltitle.setForeground(new Color(15,5,50));


        //----------------------Background Image--------------------------------------//

        newClientInfo = new JButton("Add New Client Info");

        exitClientInfo = new JButton("Show All Client Info ");

        updateClientInfo = new JButton("Update Client Info ");
        miscButton = new JButton("Miscellaneous");

        c.add(newClientInfo);
        newClientInfo.setBounds(80,150,150,90);
        newClientInfo.setForeground(Color.BLACK);
        newClientInfo.setBackground(new Color(250,128,114));
        newClientInfo.addActionListener(this);

        c.add(exitClientInfo);
        exitClientInfo.setBounds(300,150,150,90);
        exitClientInfo.setForeground(Color.BLACK);
        exitClientInfo.setBackground(new Color(250,128,114));
        exitClientInfo.addActionListener(this);

        c.add(updateClientInfo);
        updateClientInfo.setBounds(80,250,150,90);
        updateClientInfo.setForeground(Color.BLACK);
        updateClientInfo.setBackground(new Color(250,128,114));
        updateClientInfo.addActionListener(this);

        c.add(miscButton);
        miscButton.setBounds(300,250,150,90);
        miscButton.setForeground(Color.BLACK);
        miscButton.setBackground(new Color(250,128,114));
        miscButton.addActionListener(this);

        copyRight=new JLabel("<html>KAAM Software Pvt Ltd<br/>Copyright @2020 , All Rights Reserved</html>", SwingConstants.CENTER);
        c.add(copyRight);
        copyRight.setBounds(200,430,300,50);
        copyRight.setFont(f3);
        copyRight.setForeground(new Color(15,5,50));



        label3=new JLabel(new ImageIcon("images/lg_1.jpeg"));
        label3.setBounds(0,-60,500,600);
        add(label3);

        setLayout(null);
        setBounds(500,0,500,500);
        setForeground(new Color(51,153,255));
        setBackground(new Color(51,153,255));
        setVisible(true);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    }

    public void actionPerformed(ActionEvent ae) {
        Object obj = ae.getSource();

        if(obj==newClientInfo)
        {

          // new ClientInfo();
            new ClientInfo();
           // this.setVisible(false);
            //this.dispose();
        }

        if(obj==exitClientInfo)
        {

            // new ClientInfo();
            //new DisplayClientInfo();
            new DisplayWindow();
            //this.setVisible(false);
            //this.dispose();
        }

        if(obj==updateClientInfo)
        {

            // new ClientInfo();
            //new ClientUpdate();
            new ClientSearch();
            //this.setVisible(false);
            //this.dispose();
        }

        if(obj==miscButton)
        {

            JOptionPane.showMessageDialog(null,"Coming Soon");
        }
    }
}
