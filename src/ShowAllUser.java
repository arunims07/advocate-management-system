import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ShowAllUser extends JFrame {
    //JTable table ;

    TableRowSorter<DefaultTableModel> sorter;
    String[] columnNames = {"UserName", "Password", "UserType"};

    DefaultTableModel demodel = null;

    //Get DataBase Obejct and Connection Object for executing Query
    JDBCConnection jdbc = null;
    Connection con = null;

    String username= "";
    String password= "";
    String usertype= "";

    public ShowAllUser() {
        super ("AMS - Display All User Information");
        setIconImage (getToolkit().getImage ("images/IconLogo.png"));

        Container c=getContentPane();
        Color c1=new Color(5,1,88);
        Color c2=new Color(45,40,100);
        Color c3=new Color(220,0,0);
        Font f=new Font("Black Oblique",Font.BOLD,20);
        Font f1=new Font("Algerian",Font.BOLD,25);
        Font f2=new Font("Arial",Font.BOLD,15);
        c.setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        c.setBackground(new Color(245,250,255));
        GridLayout gridLayout = new GridLayout(0, 1,10,10);


        demodel = new DefaultTableModel();
        demodel.setColumnIdentifiers(columnNames);

        //Get DataBase Obejct and Connection Object for executing Query
        jdbc = new JDBCConnection();
        con = jdbc.getConnectionObject();
        try {
            String sql = "select * from login";
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                username = rs.getString("UserName");
                password = rs.getString("Password");
                usertype = rs.getString("UserType");
                demodel.addRow(new Object[]{username, password, usertype});

            }
        }
        catch(SQLException sx)
        {

        }


        JTable table = new JTable(demodel);
        table.setPreferredScrollableViewportSize(new Dimension(400, 100));
        table.setFillsViewportHeight(true);
        table.setRowSorter(sorter);

        gridLayout.addLayoutComponent("All User Details", table);

        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);

        //Add the scroll pane to this panel.
        c.add(scrollPane);


        setVisible(true);
        setBounds(500,0,500,435);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setResizable(false);
    }

}
