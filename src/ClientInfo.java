//Guru Kripa
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import javax.swing.filechooser.FileSystemView;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;


public class ClientInfo extends JFrame implements ActionListener
{
	JLabel ltitle,lcid,lcname,ladd,lloc,lcity,lstate,lcnt,lpin,lph,lmo,lemail,locc,lsex,lsallevel,lhearingDate,lNhearingDate,lCourtName,
    laddDocument,descriptionLable;
	JTextField txtcid,txtcname,txtadd,txtloc,txtcity,txtcnt,txtpin,txtsex,txtsallevel,txthearing,txtNhearing;
	JTextField txtph,txtmo,txtemail,txtocc,txtAdddocument;
	JComboBox scname,scCourtName,scCaseNature,scState;
	// Declaration of object of JRadioButton class.
	JRadioButton jRadioMale;
	// Declaration of object of JRadioButton class.
	JRadioButton jRadioFemale;
	// Declaration of object of ButtonGroup class.
	ButtonGroup G1;
    JFileChooser fileChooser;
    JButton browesBtton,saveButton,cancelButton,dateHButton,dateNButton;
	JTextArea description;

	Connection con;
	String[] sourcePath = new String[10];
	boolean morefiles ,browseButtonClicked= false;
	int fileIndex = 0;
	public ClientInfo()
	{
		super ("AMS -Client Information");
		setIconImage (getToolkit().getImage ("images/IconLogo.png"));
	
		Container c=getContentPane();
		Color c1=new Color(5,1,88);
		Color c2=new Color(45,40,100);
		Color c3=new Color(0,0,0);
		Font f=new Font("Black Oblique",Font.BOLD,20);
		Font f1=new Font("Algerian",Font.BOLD,25);
		Font f2=new Font("Arial",Font.BOLD,15);
		c.setLayout(null);
		c.setBackground(new Color(245,250,255));

		//Get DataBase Obejct and Connection Object for executing Query
		JDBCConnection jdbc = new JDBCConnection();
		con = jdbc.getConnectionObject();
		
		ltitle=new JLabel("Client's Information");
		ltitle.setBounds(new Rectangle(300,5,550,45));
		ltitle.setFont(new Font("Algerian",Font.BOLD,45));
		ltitle.setForeground(new Color(3,7,66));
		c.add(ltitle);

		JLabel li=new JLabel(new ImageIcon("images/IconLogo.png"));
		li.setBounds(0,0,50,50);
		c.add(li);

		JLabel lu=new JLabel();
		lu.setBounds(290,47,540,3);
		lu.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		c.add(lu);
		
		lcid=new JLabel("CASE NO");
		lcid.setBounds(new Rectangle(100,120,142,25));
		lcid.setFont(f);
		lcid.setForeground(c2);
		c.add(lcid);
		/////////////////////////////////////////////////
		lcname=new JLabel("Client Name");
		lcname.setBounds(new Rectangle(100,80,142,25));
		lcname.setFont(f);
		lcname.setForeground(c2);
		c.add(lcname);

		ladd=new JLabel("Address");
		ladd.setBounds(new Rectangle(100,160,142,25));
		ladd.setFont(f);
		ladd.setForeground(c2);
		c.add(ladd);

		lloc=new JLabel("Locality");
		lloc.setBounds(new Rectangle(100,200,142,25));
		lloc.setFont(f);
		lloc.setForeground(c2);
		c.add(lloc);

		lcity=new JLabel("City");
		lcity.setBounds(new Rectangle(100,240,142,25));
		lcity.setFont(f);
		lcity.setForeground(c2);
		c.add(lcity);		

		lstate=new JLabel("State");
		lstate.setBounds(new Rectangle(100,280,142,25));
		lstate.setFont(f);
		lstate.setForeground(c2);
		c.add(lstate);

		lcnt=new JLabel("Country");
		lcnt.setBounds(new Rectangle(100,320,142,25));
		lcnt.setFont(f);
		lcnt.setForeground(c2);
		c.add(lcnt);

		lpin=new JLabel("Pin Code");
		lpin.setBounds(new Rectangle(100,360,142,25));
		lpin.setFont(f);
		lpin.setForeground(c2);
		c.add(lpin);

        lCourtName=new JLabel("Distt Court");
        lCourtName.setBounds(new Rectangle(100,400,142,25));
        lCourtName.setFont(f);
        lCourtName.setForeground(c2);
        c.add(lCourtName);

        laddDocument=new JLabel("Add Document");
        laddDocument.setBounds(new Rectangle(100,440,160,25));
        laddDocument.setFont(f);
        laddDocument.setForeground(c2);
        c.add(laddDocument);

		descriptionLable = new JLabel("Description");
		descriptionLable.setBounds(new Rectangle(100,480,160,25));
		descriptionLable.setFont(f);
		descriptionLable.setForeground(c2);
		c.add(descriptionLable);


		lph=new JLabel("Phone No.");
		lph.setBounds(new Rectangle(525,80,142,25));
		lph.setFont(f);
		lph.setForeground(c2);
		c.add(lph);

		lmo=new JLabel("Mobile No.");
		lmo.setBounds(new Rectangle(525,120,142,25));
		lmo.setFont(f);
		lmo.setForeground(c2);
		c.add(lmo);

		lemail=new JLabel("Email");
		lemail.setBounds(new Rectangle(525,160,142,25));
		lemail.setFont(f);
		lemail.setForeground(c2);
		c.add(lemail);

		locc=new JLabel("Ocuupation");
		locc.setBounds(new Rectangle(525,200,142,25));
		locc.setFont(f);
		locc.setForeground(c2);
		c.add(locc);

		lsex=new JLabel("Sex");
		lsex.setBounds(new Rectangle(525,240,142,25));
		lsex.setFont(f);
		lsex.setForeground(c2);
		c.add(lsex);

		lsallevel=new JLabel("Case Nature");
		lsallevel.setBounds(new Rectangle(525,280,145,25));
		lsallevel.setFont(f);
		lsallevel.setForeground(c2);
		c.add(lsallevel);

		lhearingDate=new JLabel("First Hearing Date");
		lhearingDate.setBounds(new Rectangle(525,320,200,25));
		lhearingDate.setFont(f);
		lhearingDate.setForeground(c2);
		c.add(lhearingDate);

		lNhearingDate=new JLabel("Next Hearing Date");
		lNhearingDate.setBounds(new Rectangle(525,360,200,25));
		lNhearingDate.setFont(f);
		lNhearingDate.setForeground(c2);
		c.add(lNhearingDate);

		//**text field**//

		txtcid=new JTextField("");
		txtcid.setBounds(new Rectangle(270,120,195,25));
		txtcid.setFont(f2);
		txtcid.setForeground(c3);
		c.add(txtcid);
		
		txtcname=new JTextField();
		txtcname.setBounds(new Rectangle(270,80,195,25));
		txtcname.setFont(f2);
		txtcname.setForeground(c3);
		c.add(txtcname);
		

		txtadd=new JTextField("");
		txtadd.setBounds(new Rectangle(270,160,195,25));
		txtadd.setFont(f2);
		txtadd.setForeground(c3);
		c.add(txtadd);

		txtloc=new JTextField("");
		txtloc.setBounds(new Rectangle(270,200,195,25));
		txtloc.setFont(f2);
		txtloc.setForeground(c3);
		c.add(txtloc);

		txtcity=new JTextField("");
		txtcity.setBounds(new Rectangle(270,240,195,25));
		txtcity.setFont(f2);
		txtcity.setForeground(c3);
		c.add(txtcity);

        // Initialize the list with items
        String[] state = new String[31];
        try{
            Statement	cs=con.createStatement();
            ResultSet rs=cs.executeQuery("select * from State");
            int i=0;
            while(rs.next()) {
                state[i] = rs.getString(2);
                i++;
            }
        }
        catch(SQLException sq)
        {

        }

        scState=new JComboBox(state);
        scState.setSelectedItem(0);
        scState.setOpaque(false);
        scState.setBounds(270,280,195,25);
        scState.setFont(f2);
        scState.setForeground(c2);
        c.add(scState);
        scState.addActionListener(this);

		txtcnt=new JTextField("India");
		txtcnt.setBounds(new Rectangle(270,320,195,25));
		txtcnt.setFont(f2);
		txtcnt.setForeground(c3);
		c.add(txtcnt);

		txtpin=new JTextField("");
		txtpin.setBounds(new Rectangle(270,360,195,25));
		txtpin.setFont(f2);
		txtpin.setForeground(c3);
		c.add(txtpin);

        // Initialize the list with items
        String[] items = new String[80];
		try{
			Statement	cs=con.createStatement();
			ResultSet rs=cs.executeQuery("select * from Court");
			int i=0;
			while(rs.next()) {
				items[i] = rs.getString(2);
				i++;
			}
		}
		catch(SQLException sq)
		{

		}

        scCourtName=new JComboBox(items);
        scCourtName.setOpaque(false);
        scCourtName.setBounds(270,400,195,25);
        scCourtName.setFont(f2);
        scCourtName.setForeground(c2);
        c.add(scCourtName);
        scCourtName.addActionListener(this);


        // make an object of the class filechooser

        browesBtton=new JButton("Browse");
        browesBtton.setBounds(new Rectangle(575,440,100,25));
        browesBtton.setFont(f2);
        browesBtton.setForeground(c3);
        c.add(browesBtton);
        browesBtton.addActionListener(this);

        txtsallevel=new JTextField();
        txtsallevel.setBounds(new Rectangle(270,440,300,25));
        txtsallevel.setFont(f2);
        txtsallevel.setForeground(c3);
        c.add(txtsallevel);

		description = new JTextArea();
		description.setFont(f2);
		description.setForeground(c3);
		JScrollPane scrollpaneText = new JScrollPane(description,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollpaneText.setBounds(new Rectangle(270,480,450,130));
		c.add(scrollpaneText);

		txtph=new JTextField("");
		txtph.setBounds(new Rectangle(750,80,195,25));
		txtph.setFont(f2);
		txtph.setForeground(c3);
		c.add(txtph);

		txtmo=new JTextField("");
		txtmo.setBounds(new Rectangle(750,120,195,25));
		txtmo.setFont(f2);
		txtmo.setForeground(c3);
		c.add(txtmo);

		txtemail=new JTextField("");
		txtemail.setBounds(new Rectangle(750,160,195,25));
		txtemail.setFont(f2);
		txtemail.setForeground(c3);
		c.add(txtemail);

		txtocc=new JTextField("");
		txtocc.setBounds(new Rectangle(750,200,195,25));
		txtocc.setFont(f2);
		txtocc.setForeground(c3);
		c.add(txtocc);

		// Initialization of object of "JRadioButton" class.
		jRadioMale = new JRadioButton();
		// Initialization of object of "JRadioButton" class.
		jRadioFemale = new JRadioButton();
		// Initialization of object of "ButtonGroup" class.
		G1 = new ButtonGroup();
		// setText(...) function is used to set text of radio button.
		// Setting text of "jRadioButton2".
		jRadioMale.setText("Male");
		jRadioMale.setActionCommand("Male");

		// Setting text of "jRadioButton4".
		jRadioFemale.setText("Female");
		jRadioFemale.setActionCommand("Female");
		// Setting Bounds of "jRadioButton2".
		jRadioMale.setBounds(new Rectangle(750,240,150,25));
		// Setting Bounds of "jRadioButton4".
		jRadioFemale.setBounds(new Rectangle(850,240,150,25));
		// Adding "jRadioButton1" and "jRadioButton3" in a Button Group "G2".
		G1.add(jRadioMale);
		G1.add(jRadioFemale);
		jRadioMale.setForeground(c3);
		c.add(jRadioMale);
		jRadioFemale.setForeground(c3);
		c.add(jRadioFemale);


		// Initialize the list with items
		String[] caseNature = new String[80];
		try{
			Statement	cs=con.createStatement();
			ResultSet rs=cs.executeQuery("select * from CaseNature");
			int i=0;
			while(rs.next()) {
                caseNature[i] = rs.getString(2);
				i++;
			}
		}
		catch(SQLException sq)
		{

		}

        scCaseNature=new JComboBox(caseNature);
        scCaseNature.setOpaque(false);
        scCaseNature.setBounds(750,280,195,25);
        scCaseNature.setFont(f2);
        scCaseNature.setForeground(c2);
        c.add(scCaseNature);
        scCaseNature.addActionListener(this);

		txthearing=new JTextField();
		txthearing.setBounds(new Rectangle(750,320,100,25));
		txthearing.setFont(f2);
		txthearing.setForeground(c3);
		txthearing.setEditable(false);
		c.add(txthearing);

		dateHButton = new JButton("First Hearing Date");
        dateHButton.setBounds(new Rectangle(860,320,180,25));
        dateHButton.setFont(f2);
        dateHButton.setForeground(c3);
        c.add(dateHButton);
        dateHButton.addActionListener(this);

		txtNhearing=new JTextField();
		txtNhearing.setBounds(new Rectangle(750,360,100,25));
		txtNhearing.setFont(f2);
		txtNhearing.setForeground(c3);
		txtNhearing.setEditable(false);
		c.add(txtNhearing);

        dateNButton = new JButton("Next Hearing Date");
        dateNButton.setBounds(new Rectangle(860,360,180,25));
        dateNButton.setFont(f2);
        dateNButton.setForeground(c3);
        c.add(dateNButton);
        dateNButton.addActionListener(this);
				
	//------------------------search by name---------------------------------------//

		Color color = new Color(0,0, 0);



        saveButton=new JButton("Save");
        saveButton.setBounds(new Rectangle(800,550,100,25));
        saveButton.setFont(f2);
        saveButton.setForeground(c3);
        c.add(saveButton);
        saveButton.addActionListener(this);

        cancelButton=new JButton("Cancel");
        cancelButton.setBounds(new Rectangle(920,550,100,25));
        cancelButton.setFont(f2);
        cancelButton.setForeground(c3);
        c.add(cancelButton);
        cancelButton.addActionListener(this);
		
		//------------------------------------------------------------------------------------------------//
		
		JLabel lbd=new JLabel();
		lbd.setBounds(0,625,1033,100);
		//lbd.setOpaque(true);
		//Color color = new Color(20, 204, 50);
    		lbd.setBorder(BorderFactory.createLineBorder(color, 1));
		lbd.setBackground(new Color(222,222,222));
		//c.add(lbd);
		
		JLabel li1=new JLabel(new ImageIcon("images/h2.jpg"));
		li1.setBounds(0,625,150,100);
		//lbd.setOpaque(true);
		Color color1 = new Color(200, 20, 50);
    		li1.setBorder(BorderFactory.createLineBorder(color1, 1));
		li1.setBackground(new Color(222,222,222));
		c.add(li1);
		
		JLabel li2=new JLabel(new ImageIcon("images/h2.jpg"));
		li2.setBounds(882,625,150,100);
		
		//lbd.setOpaque(true);
		//Color color = new Color(20,204, 50);
    		li2.setBorder(BorderFactory.createLineBorder(color1, 1));
		li2.setBackground(new Color(222,222,222));
		c.add(li2);
	

		
	//-------------------------------------BUTTON-----------------------------------------------------------------------//
	
		
		JLabel lbd1=new JLabel(new ImageIcon("images/t1.jpg"));
		lbd1.setBounds(395,625,165,90);
		c.add(lbd1);
		
		JPanel pp=new JPanel();
		pp.setBounds(0,625,1033,100);
		pp.add(lbd);
		pp.setBackground(new Color(150,150,150));
		c.add(pp);
		
	//---------------------------------------------------------------------------------------------------------------------------------------//


		setVisible(true);
		setBounds(160,75,1035,750);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);



	}

	//**************************************LISTENER*************************************************************//
	
	public void actionPerformed(ActionEvent ae)
	{
		Object obj=ae.getSource();
		

		try {
            if (obj == browesBtton) {
                selectFile(-1);
                browseButtonClicked = true;
			}

				if (obj == saveButton)
					{

					    //Check All Length and Text Filed before Processing
                        if(txtcname.getText() == "" ||
                                txtadd.getText()=="" ||
                                txtloc.getText() == "" ||
                                txtcnt.getText() == "" ||
                                txtcity.getText() == "" ||
                                txtpin.getText() == "" ||
                                txtmo.getText().length() < 10 ||
                                txtemail.getText() == "" ||
                                txtocc.getText() == "" ||
                                txthearing.getText() == "" ||
                                txtcid.getText() == "")
                        {
                            JOptionPane.showMessageDialog(null,"Please fill all required data");
                            return;
                        }

                        Statement	cs=con.createStatement();

                        cs.executeQuery("select * from ClientInfo");
                        // the mysql insert statement
                        String query = " INSERT INTO ClientInfo (ClientName,Address,Locality,City,State,Country,PCode,CourtName,MobileNo,Email,Occupation,Sex,CaseNature,TodayHD, NextHD, CaseNo,Description)"
                                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ?, ?, ?, ?, ? ,?,?)";

                        // create the mysql insert preparedstatement
                        PreparedStatement preparedStmt = con.prepareStatement(query);
                        preparedStmt.setString (1, txtcname.getText());
                        preparedStmt.setString (2, txtadd.getText());
                        preparedStmt.setString (3, txtloc.getText());
                        preparedStmt.setString (4, txtcity.getText());
                        preparedStmt.setString (5, scState.getSelectedItem().toString());
                        preparedStmt.setString (6, txtcnt.getText());
                        preparedStmt.setString (7, txtpin.getText());
                        preparedStmt.setString (8, scCourtName.getSelectedItem().toString());
                        preparedStmt.setString (9, txtmo.getText());
                        preparedStmt.setString (10, txtemail.getText());
                        preparedStmt.setString (11, txtocc.getText());
                        preparedStmt.setString (12, G1.getSelection().getActionCommand());
                        preparedStmt.setString (13, scCaseNature.getSelectedItem().toString());
                        preparedStmt.setString   (14, txthearing.getText());
                        preparedStmt.setString(15, txtNhearing.getText());
                        preparedStmt.setString    (16, txtcid.getText());
                        preparedStmt.setString    (17, description.getText());


                        // execute the preparedstatement
                        preparedStmt.execute();

                        if(browseButtonClicked) {
                            ResultSet rs = cs.executeQuery("select * from ClientInfo ORDER BY ClientID DESC LIMIT 1");
                            rs.next();
                            String destinationPath = System.getProperty("user.dir");

                            String dirName = txtcname.getText() + "_" + rs.getString("ClientId");
                            File theDir = new File(dirName);

                            // if the directory does not exist, create it
                            if (!theDir.exists()) {
                                System.out.println("Creating directory: " + theDir.getName());
                                boolean result = false;
                                try {
                                    theDir.mkdir();
                                    result = true;
                                } catch (SecurityException se) {
                                    //handle it
                                }
                                if (result) {
                                    System.out.println("DIR created");
                                }
                            }


                            destinationPath = destinationPath + File.separator + txtcname.getText() + "_" + rs.getString("ClientId");
                            System.out.println("Dest Path :  " + destinationPath);
                            System.out.println("Before File Copy  " + fileIndex);
                            for (int fileId = 0; fileId <= fileIndex; fileId++) {
                                Path src = Paths.get(sourcePath[fileId]);
                                System.out.println("File ID : " + fileId + "File IMage " + sourcePath[fileId]);
                                Path dest = Paths.get(destinationPath);
                                Files.copy(src, dest.resolve(src.getFileName()), REPLACE_EXISTING);
                            }
                        }

						JOptionPane.showMessageDialog(null,"Data Saved Successfully");
						morefiles = false;
                        browseButtonClicked = false;
						fileIndex = 0;
                        txtclear();
					}

					if (obj == cancelButton)
						{
							JOptionPane.showMessageDialog(null,"Are you Sure to Cancel ");
                            this.setVisible(false);
                            this.dispose();
						}

            if (obj == dateHButton)
            {
                txthearing.setText(new DatePicker(this).setPickedDate());
            }

            if (obj == dateNButton)
            {
                txtNhearing.setText(new DatePicker(this).setPickedDate());
            }


        }catch(Exception e)
        {
            System.out.println(e);
            JOptionPane.showMessageDialog(null,"Data can not be Updated");
        }

		

	}


    /**
     *
     * @param joptionValue
     */
	public void selectFile(int joptionValue)
    {
            int localJoptionValue = joptionValue;
            fileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory()); //Downloads Directory as default
            fileChooser.setDialogTitle("Select Location");
            int eventID = fileChooser.showSaveDialog(null);
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setMultiSelectionEnabled(true);

            if (eventID == JFileChooser.APPROVE_OPTION) {
                txtsallevel.setText(fileChooser.getSelectedFile().getPath());

                if (!morefiles) {
                    sourcePath[fileIndex] = fileChooser.getSelectedFile().getPath();
                    System.out.println("Enter First Time  " + fileIndex);
                    System.out.println(sourcePath[fileIndex]);
                    morefiles = true;

                }
                else
                {
                    fileIndex++;
                    sourcePath[fileIndex] = fileChooser.getSelectedFile().getPath();
                    System.out.println("While more File  " + fileIndex);
                    System.out.println(sourcePath[fileIndex]);
                }

                localJoptionValue = JOptionPane.showConfirmDialog(null, "Do you want to add more file");

                if (localJoptionValue == 0) {
                    selectFile(joptionValue);
                } else {
                    txtsallevel.setEditable(false);
                    browesBtton.setEnabled(false);
                }

            }
    }

	//*---------------------------------------------------------------------------------------------------------------------------------*//
	
	private void txtclear()
	{
		
		txtcid.setText("");
		txtcname.setText("");
		txtadd.setText("");

		txtloc.setText("");
		txtcity.setText("");

		txtcnt.setText("");
		txtpin.setText("");
		//txtph.setText("");

		txtmo.setText("");
		txtemail.setText("");
		txtocc.setText("");
		//txtsex.setText("");
		//txtsallevel.setText("");

	}

}