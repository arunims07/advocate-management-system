import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class DisplayClientFullInfo extends JFrame implements ActionListener, Printable
{
    JLabel ltitle,lcid,lcname,ladd,lloc,lcity,lstate,lcnt,lpin,lph,lmo,lemail,locc,lsex,lsallevel,lhearingDate,lNhearingDate,lCourtName,
            laddDocument,descriptionLable;
    JTextField txtcid,txtcname,txtadd,txtloc,txtcity,txtstate,txtcnt,txtpin,scCourtName,txtsallevel,txthearing,txtNhearing,scCaseNature;
    JTextField txtph,txtmo,txtemail,txtocc,txtSex;
    JFileChooser fileChooser;
    JButton downloadBtton,okButton,printButton;
    JTextArea description;

    Connection con;
    JDBCConnection jdbc = null;
    String clientId ,clientName= "";

    public DisplayClientFullInfo(String caseNo)
    {
        super ("AMS - Display Full Client Information");
        setIconImage (getToolkit().getImage ("images/IconLogo.png"));

        Container c=getContentPane();
        Color c1=new Color(5,1,88);
        Color c2=new Color(45,40,100);
        Color c3=new Color(0,0,0);
        Font f=new Font("Black Oblique",Font.BOLD,20);
        Font f1=new Font("Algerian",Font.BOLD,25);
        Font f2=new Font("Arial",Font.BOLD,15);
        c.setLayout(null);
        c.setBackground(new Color(245,250,255));

        ltitle=new JLabel("Client's Details Information");
        ltitle.setBounds(new Rectangle(300,5,550,45));
        ltitle.setFont(new Font("Algerian",Font.BOLD,45));
        ltitle.setForeground(new Color(3,7,66));
        c.add(ltitle);

        JLabel li=new JLabel(new ImageIcon("images/IconLogo.png"));
        li.setBounds(0,0,50,50);
        c.add(li);

        JLabel lu=new JLabel();
        lu.setBounds(290,47,540,3);
        lu.setBorder(BorderFactory.createLineBorder(Color.black, 2));
        c.add(lu);

        lcid=new JLabel("CASE NO");
        lcid.setBounds(new Rectangle(100,120,142,25));
        lcid.setFont(f);
        lcid.setForeground(c2);
        c.add(lcid);
        /////////////////////////////////////////////////
        lcname=new JLabel("Client Name");
        lcname.setBounds(new Rectangle(100,80,142,25));
        lcname.setFont(f);
        lcname.setForeground(c2);
        c.add(lcname);

        ladd=new JLabel("Address");
        ladd.setBounds(new Rectangle(100,160,142,25));
        ladd.setFont(f);
        ladd.setForeground(c2);
        c.add(ladd);

        lloc=new JLabel("Locality");
        lloc.setBounds(new Rectangle(100,200,142,25));
        lloc.setFont(f);
        lloc.setForeground(c2);
        c.add(lloc);

        lcity=new JLabel("City");
        lcity.setBounds(new Rectangle(100,240,142,25));
        lcity.setFont(f);
        lcity.setForeground(c2);
        c.add(lcity);

        lstate=new JLabel("State");
        lstate.setBounds(new Rectangle(100,280,142,25));
        lstate.setFont(f);
        lstate.setForeground(c2);
        c.add(lstate);

        lcnt=new JLabel("Country");
        lcnt.setBounds(new Rectangle(100,320,142,25));
        lcnt.setFont(f);
        lcnt.setForeground(c2);
        c.add(lcnt);

        lpin=new JLabel("Pin Code");
        lpin.setBounds(new Rectangle(100,360,142,25));
        lpin.setFont(f);
        lpin.setForeground(c2);
        c.add(lpin);

        lCourtName=new JLabel("Distt Court");
        lCourtName.setBounds(new Rectangle(100,400,142,25));
        lCourtName.setFont(f);
        lCourtName.setForeground(c2);
        c.add(lCourtName);

        laddDocument=new JLabel("Download Document");
        laddDocument.setBounds(new Rectangle(100,440,160,25));
        laddDocument.setFont(f);
        laddDocument.setForeground(c2);
        c.add(laddDocument);

        descriptionLable = new JLabel("Description");
        descriptionLable.setBounds(new Rectangle(100,480,160,25));
        descriptionLable.setFont(f);
        descriptionLable.setForeground(c2);
        c.add(descriptionLable);


        lph=new JLabel("Phone No.");
        lph.setBounds(new Rectangle(525,80,142,25));
        lph.setFont(f);
        lph.setForeground(c2);
        c.add(lph);

        lmo=new JLabel("Mobile No.");
        lmo.setBounds(new Rectangle(525,120,142,25));
        lmo.setFont(f);
        lmo.setForeground(c2);
        c.add(lmo);

        lemail=new JLabel("Email");
        lemail.setBounds(new Rectangle(525,160,142,25));
        lemail.setFont(f);
        lemail.setForeground(c2);
        c.add(lemail);

        locc=new JLabel("Ocuupation");
        locc.setBounds(new Rectangle(525,200,142,25));
        locc.setFont(f);
        locc.setForeground(c2);
        c.add(locc);

        lsex=new JLabel("Sex");
        lsex.setBounds(new Rectangle(525,240,142,25));
        lsex.setFont(f);
        lsex.setForeground(c2);
        c.add(lsex);

        lsallevel=new JLabel("Case Nature");
        lsallevel.setBounds(new Rectangle(525,280,145,25));
        lsallevel.setFont(f);
        lsallevel.setForeground(c2);
        c.add(lsallevel);

        lhearingDate=new JLabel("First Hearing Date");
        lhearingDate.setBounds(new Rectangle(525,320,200,25));
        lhearingDate.setFont(f);
        lhearingDate.setForeground(c2);
        c.add(lhearingDate);

        lNhearingDate=new JLabel("Next Hearing Date");
        lNhearingDate.setBounds(new Rectangle(525,360,200,25));
        lNhearingDate.setFont(f);
        lNhearingDate.setForeground(c2);
        c.add(lNhearingDate);

        //**text field**//

        txtcid=new JTextField("");
        txtcid.setBounds(new Rectangle(270,120,195,25));
        txtcid.setFont(f2);
        txtcid.setForeground(c3);
        txtcid.setEditable(false);
        c.add(txtcid);

        txtcname=new JTextField();
        txtcname.setBounds(new Rectangle(270,80,195,25));
        txtcname.setFont(f2);
        txtcname.setForeground(c3);
        txtcname.setEditable(false);
        c.add(txtcname);


        txtadd=new JTextField("");
        txtadd.setBounds(new Rectangle(270,160,195,25));
        txtadd.setFont(f2);
        txtadd.setForeground(c3);
        txtadd.setEditable(false);
        c.add(txtadd);

        txtloc=new JTextField("");
        txtloc.setBounds(new Rectangle(270,200,195,25));
        txtloc.setFont(f2);
        txtloc.setForeground(c3);
        txtloc.setEditable(false);
        c.add(txtloc);

        txtcity=new JTextField("");
        txtcity.setBounds(new Rectangle(270,240,195,25));
        txtcity.setFont(f2);
        txtcity.setForeground(c3);
        txtcity.setEditable(false);
        c.add(txtcity);

        txtstate=new JTextField("");
        txtstate.setBounds(new Rectangle(270,280,195,25));
        txtstate.setFont(f2);
        txtstate.setForeground(c3);
        txtstate.setEditable(false);
        c.add(txtstate);

        txtcnt=new JTextField("");
        txtcnt.setBounds(new Rectangle(270,320,195,25));
        txtcnt.setFont(f2);
        txtcnt.setForeground(c3);
        txtcnt.setEditable(false);
        c.add(txtcnt);

        txtpin=new JTextField("");
        txtpin.setBounds(new Rectangle(270,360,195,25));
        txtpin.setFont(f2);
        txtpin.setForeground(c3);
        txtpin.setEditable(false);
        c.add(txtpin);

        scCourtName=new JTextField("");
        scCourtName.setBounds(new Rectangle(270,400,195,25));
        scCourtName.setFont(f2);
        scCourtName.setForeground(c3);
        scCourtName.setEditable(false);
        c.add(scCourtName);


        // make an object of the class filechooser

        downloadBtton=new JButton("Download Document");
        downloadBtton.setBounds(new Rectangle(575,440,200,25));
        downloadBtton.setFont(f2);
        downloadBtton.setForeground(c3);
        c.add(downloadBtton);
        downloadBtton.addActionListener(this);

        txtsallevel=new JTextField();
        txtsallevel.setBounds(new Rectangle(270,440,300,25));
        txtsallevel.setFont(f2);
        txtsallevel.setForeground(c3);
        txtsallevel.setEditable(false);
        c.add(txtsallevel);


        description = new JTextArea();
        description.setFont(f2);
        description.setForeground(c3);
        JScrollPane scrollpaneText = new JScrollPane(description,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollpaneText.setBounds(new Rectangle(270,480,450,130));
        c.add(scrollpaneText);

        txtph=new JTextField("");
        txtph.setBounds(new Rectangle(750,80,195,25));
        txtph.setFont(f2);
        txtph.setForeground(c3);
        txtph.setEditable(false);
        c.add(txtph);

        txtmo=new JTextField("");
        txtmo.setBounds(new Rectangle(750,120,195,25));
        txtmo.setFont(f2);
        txtmo.setForeground(c3);
        txtmo.setEditable(false);
        c.add(txtmo);

        txtemail=new JTextField("");
        txtemail.setBounds(new Rectangle(750,160,195,25));
        txtemail.setFont(f2);
        txtemail.setForeground(c3);
        txtemail.setEditable(false);
        c.add(txtemail);

        txtocc=new JTextField("");
        txtocc.setBounds(new Rectangle(750,200,195,25));
        txtocc.setFont(f2);
        txtocc.setForeground(c3);
        txtocc.setEditable(false);
        c.add(txtocc);

        txtSex=new JTextField("");
        txtSex.setBounds(new Rectangle(750,240,195,25));
        txtSex.setFont(f2);
        txtSex.setForeground(c3);
        txtSex.setEditable(false);
        c.add(txtSex);


        scCaseNature=new JTextField("");
        scCaseNature.setBounds(new Rectangle(750,280,195,25));
        scCaseNature.setFont(f2);
        scCaseNature.setForeground(c3);
        scCaseNature.setEditable(false);
        c.add(scCaseNature);

        txthearing=new JTextField();
        txthearing.setBounds(new Rectangle(750,320,195,25));
        txthearing.setFont(f2);
        txthearing.setForeground(c3);
        txthearing.setEditable(false);
        c.add(txthearing);

        txtNhearing=new JTextField();
        txtNhearing.setBounds(new Rectangle(750,360,195,25));
        txtNhearing.setFont(f2);
        txtNhearing.setForeground(c3);
        txtNhearing.setEditable(false);
        c.add(txtNhearing);

        //------------------------search by name---------------------------------------//


        Color color = new Color(0,0, 0);

        okButton=new JButton("OK");
        okButton.setBounds(new Rectangle(800,550,100,25));
        okButton.setFont(f2);
        okButton.setForeground(c3);
        c.add(okButton);
        okButton.addActionListener(this);

        printButton=new JButton("Print");
        printButton.setBounds(new Rectangle(900,550,100,25));
        printButton.setFont(f2);
        printButton.setForeground(c3);
        c.add(printButton);
        printButton.addActionListener(this);

        //------------------------------------------------------------------------------------------------//

        JLabel lbd=new JLabel();
        lbd.setBounds(0,625,1033,100);
        lbd.setBorder(BorderFactory.createLineBorder(color, 1));
        lbd.setBackground(new Color(222,222,222));

        JLabel li1=new JLabel(new ImageIcon("images/h2.jpg"));
        li1.setBounds(0,625,150,100);
        Color color1 = new Color(200, 20, 50);
        li1.setBorder(BorderFactory.createLineBorder(color1, 1));
        li1.setBackground(new Color(222,222,222));
        c.add(li1);

        JLabel li2=new JLabel(new ImageIcon("images/h2.jpg"));
        li2.setBounds(882,625,150,100);
        li2.setBorder(BorderFactory.createLineBorder(color1, 1));
        li2.setBackground(new Color(222,222,222));
        c.add(li2);



        //-------------------------------------BUTTON-----------------------------------------------------------------------//


        JLabel lbd1=new JLabel(new ImageIcon("images/t1.jpg"));
        lbd1.setBounds(395,625,165,90);
        c.add(lbd1);

        JPanel pp=new JPanel();
        pp.setBounds(0,625,1033,100);
        pp.add(lbd);
        pp.setBackground(new Color(150,150,150));
        c.add(pp);

        //---------------------------------------------------------------------------------------------------------------------------------------//


        setVisible(true);
        setBounds(160,75,1035,750);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setResizable(false);


        try
        {
            //Get DataBase Obejct and Connection Object for executing Query
            jdbc = new JDBCConnection();
            con = jdbc.getConnectionObject();
            String sql = "select * from ClientInfo where CaseNo = " + "\"" + caseNo + "\"";
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs1 = ps.executeQuery();

            rs1.next();
            clientId = rs1.getString(1);
            clientName = rs1.getString(2);
            txtcname.setText(rs1.getString(2));
            txtadd.setText(rs1.getString(3));
            txtloc.setText(rs1.getString(4));
            txtcity.setText(rs1.getString(5));
            txtstate.setText(rs1.getString(6));
            txtcnt.setText(rs1.getString(7));
            txtpin.setText(rs1.getString(8));
            scCourtName.setText(rs1.getString(9));
            txtmo.setText(rs1.getString(10));
            txtemail.setText(rs1.getString(11));
            txtocc.setText(rs1.getString(12));
            txtSex.setText(rs1.getString(13));
            scCaseNature.setText(rs1.getString(14));
            txthearing.setText(rs1.getString(15));
            txtNhearing.setText(rs1.getString(16));
            txtcid.setText(rs1.getString(17));
            description.setText(rs1.getString(18));
        }

        catch(Exception e)
        {
            System.out.println(e);
            JOptionPane.showMessageDialog(null,"data can not be fatched");
        }



        //-------------------------------------------Last line of the Constructor--clientdetail()------------------------------------------------------------//
    }

    public int print(Graphics g, PageFormat pf, int page) throws PrinterException {

        if (page > 0) { /* We have only one page, and 'page' is zero-based */
            return NO_SUCH_PAGE;
        }

        /*
         * User (0,0) is typically outside the imageable area, so we must translate
         * by the X and Y values in the PageFormat to avoid clipping
         */
        Graphics2D g2d = (Graphics2D) g;
        g2d.translate(pf.getImageableX(), pf.getImageableY());

        /* Now print the window and its visible contents */
        this.printAll(g);

        /* tell the caller that this page is part of the printed document */
        return PAGE_EXISTS;
    }
    //**************************************LISTENER*************************************************************//

    public void actionPerformed(ActionEvent ae)
    {
        Object obj=ae.getSource();

        try {
            if (obj == downloadBtton) {
                fileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory()); //Downloads Directory as default
                fileChooser.setDialogTitle("Select Location");
                //int eventID = fileChooser.showSaveDialog(null);
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileChooser.setAcceptAllFileFilterUsed(false);
                int eventID = fileChooser.showOpenDialog(null);

                if (eventID == JFileChooser.APPROVE_OPTION) {
                    //fileID = fileChooser.getSelectedFile().getPath();
                    txtsallevel.setText(fileChooser.getSelectedFile().getPath());
                    System.out.println(fileChooser.getSelectedFile().getPath());
                }

                String sourcePath = System.getProperty("user.dir");
                String destinationPath = fileChooser.getSelectedFile().getPath();
                System.out.println(destinationPath);

                sourcePath = sourcePath + File.separator + clientName + "_" + clientId;
                System.out.println(sourcePath);
                File SourceDir = new File(sourcePath);
                for(File srcFile : SourceDir.listFiles()) {
                    Path src = Paths.get(srcFile.getPath());
                    Path dest = Paths.get(destinationPath);
                    Files.copy(src, dest.resolve(src.getFileName()), REPLACE_EXISTING);
                }

            }
            if(obj == okButton)
            {
                this.setVisible(false);
                this.dispose();

            }

            if(obj == printButton)
            {
                PrinterJob job = PrinterJob.getPrinterJob();
                job.setPrintable(this);
                boolean ok = job.printDialog();
                if (ok) {
                    try {
                        job.print();
                    } catch (PrinterException ex) {
                        /* The job did not successfully complete */
                    }
                }
            }
        }catch(Exception e)
        {
            System.out.println(e);
            JOptionPane.showMessageDialog(null,"data can not be fatched");
        }



    }



}