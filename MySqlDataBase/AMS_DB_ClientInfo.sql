-- MySQL dump 10.13  Distrib 8.0.19, for macos10.15 (x86_64)
--
-- Host: localhost    Database: AMS_DB
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ClientInfo`
--

DROP TABLE IF EXISTS `ClientInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ClientInfo` (
  `ClientId` int NOT NULL AUTO_INCREMENT,
  `ClientName` varchar(45) NOT NULL,
  `Address` varchar(45) DEFAULT NULL,
  `Locality` varchar(45) DEFAULT NULL,
  `City` varchar(45) DEFAULT NULL,
  `State` varchar(45) DEFAULT NULL,
  `Country` varchar(45) DEFAULT NULL,
  `PCode` varchar(45) NOT NULL,
  `CourtName` varchar(45) DEFAULT NULL,
  `MobileNo` varchar(45) NOT NULL,
  `Email` varchar(45) DEFAULT NULL,
  `Occupation` varchar(45) DEFAULT NULL,
  `Sex` varchar(45) NOT NULL,
  `CaseNature` varchar(45) DEFAULT NULL,
  `TodayHD` varchar(45) NOT NULL,
  `NextHD` varchar(45) NOT NULL,
  `CaseNo` varchar(45) NOT NULL,
  `Description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ClientId`),
  UNIQUE KEY `CaseNo_UNIQUE` (`CaseNo`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ClientInfo`
--

LOCK TABLES `ClientInfo` WRITE;
/*!40000 ALTER TABLE `ClientInfo` DISABLE KEYS */;
INSERT INTO `ClientInfo` VALUES (1,'Arun Srivastav','SBI','Bank Road','Anand Nagar','UP','India','273155','Mahrajgunj','1234567890','xyz@gmail.com','Service','Male','Criminal','2020-02-12 10:00:00','2020-02-15 10:00:01','12',NULL),(2,'Ashihs','Yalnka','Hnnur','Banglore','Karntka','India','560102','Banglore','1234567890','xyz@gmail.com','Service','Male','Criminal','2020-02-28','2020-03-28','1234',NULL),(3,'Akhilesh','Farenda','Near SBI','Anand Nagar','UP','India','273155','Maharaj Gunj','1234567098','abc@gmail.com','Govt Service','Male','Normal','28-02-2020','14-03-2020','1231',NULL),(4,'ClientName','Address','Locality','City','State','Country','PCode','CourtName','MobileNo','Email','Occupation','Sex','CaseNature','22-02-2020','23-02-2020','1230',NULL),(5,'Mukesh','Farenda','Near SBI','Jhunwa','UP','India','273155','Maharaj Gunj','1234567098','abc@gmail.com','Govt Service','Male','Normal','22-02-2020','14-03-2020','1232',NULL),(6,'Ajay','Gorakhpur','Near SBI','Gazipur','UP','India','273155','Maharaj Gunj','1234567098','abc@gmail.com','Govt Service','Male','Normal','22-02-2020','14-03-2020','1254',NULL),(7,'Puru','Ranidiha','Near SBI','Gorakhpur','UP','India','273155','Allahbaad','1234567788','abc@gmail.com','Govt Service','Male','Normal','22-02-2020','14-03-2020','1257',NULL),(8,'Tarak','Banglore','Near SBI','HSR','UP','India','273155','Delhi','1234567090','abc@gmail.com','Govt Service','Male','Normal','22-02-2020','14-03-2020','1255',NULL),(9,'Sanjay','Electronic City','Phase1','Banglore','UP','India','560102','Agra','6789054321','ert@gmail.com','Service','Male','Criminal','24-02-2020','25-02-2020','6789',NULL),(10,'Festus','Electronic City','Phase 2','Banglore','UP','India','560102','Agra','2134567890','sdr@gmail.com','Service','Male','Criminal','24-02-2020','25-02-2020','6785',NULL),(11,'Ganesh','HSR','Sector1','Banglore','UP','India','560102','Agra','12432378906','ser@gmail.com','Service','Male','Criminal','25-02-2020','28-02-2020','3344',NULL),(12,'Vinodh','Electronic City','HSR Sector 6','Banglore','UP','India','560102','Gonda','0987654321','vin@gmail.com','Service','Male','Criminal','25-02-2020','27-02-2020','7788',NULL),(13,'Ram Mohan','Kuzodiki','HSR','Kerla','UP','India','560102','Allahabad','32451178','se','Service','Male','Criminal','25-02-2020','26-02-2020','4567',NULL),(15,'Ramo','fjh','fokfj','Banglore','UP','India','4344','Agra','90','afkfo','Service ','Male','Criminal','24-02-2020','26-02-2020','3456',NULL),(16,'Golu','Nawtnwa','Mahrajgunj','Mahraj Gunj','Uttar Pradesh','India','2345','Maharajganj','2345','sde','Service','Male','Criminal','23-02-2020','24-02-2020','9011',NULL),(17,'Manish','nam','ajs','Mahrajgunj','Uttar Pradesh','India','234','Maharajganj','123','asd','Service','Male','Criminal','23-02-2020','24-02-2020','1111',''),(18,'Hello','gdg','xbb','ffgf','Uttar Pradesh','India','45646','Agra','3445','chhdd','cnn','Male','Criminal','23-02-2020','24-02-2020','1010',''),(19,'hell1','jjn','vvb','ddg','Uttar Pradesh','India','46','Agra','6464','jdnvjk','Service ','Male','Criminal','23-02-2020','24-02-2020','3333',''),(20,'Hello2','kjkj','jbbj','ih','Uttar Pradesh','India','465','Agra','355','jbk','bjh','Male','Criminal','23-02-2020','25-02-2020','2222','gddfggb'),(21,'Hello3','gfg','fghghh','fhff','Uttar Pradesh','India','466','Allahabad','5545','hhh','gnfgh','Male','Criminal','23-02-2020','24-02-2020','4444',''),(22,'Hello4','gdfggd','cbb','dgd','Uttar Pradesh','India','466','Amethi','4646','xfdbf','nfgb','Male','Criminal','24-02-2020','27-02-2020','5555','sgdgg'),(23,'Hello9','dfgd','dfgd','vxgsgs','Uttar Pradesh','India','465','Azamgarh','355636','gfdfg','sgsg','Male','Criminal','23-02-2020','25-02-2020','9999','gdhhh'),(24,'Hello96','sfgg','dbddf','dgg','Uttar Pradesh','India','457','Amroha','355366','svvqvvf','ggg','Male','Criminal','23-02-2020','24-02-2020','2101','dfssg'),(25,'Hello6E','dbdf','dhdh','dfbdh','Uttar Pradesh','India','6465','Auraiya','5335','dhdh','rthrh','Male','Criminal','23-02-2020','24-02-2020','9090','hfhfg'),(26,'Hello50','dhdh','fhfh','fhh','Uttar Pradesh','India','747','Ambedkar Nagar at Akbarpur','4747','dhhq','fhfhf','Male','Criminal','23-02-2020','24-02-2020','8888','dhfh'),(27,'Hello20','gg','fd','dhh','Uttar Pradesh','India','466','Barabanki','35335','gdfg','fff','Male','Criminal','23-02-2020','25-02-2020','12345','dggge'),(28,'Hello45','wttt','fnfh','fghfh','Uttar Pradesh','India','7577','Balrampur','7747','rhrhr','fghfhf','Male','Criminal','23-02-2020','24-02-2020','345678','fgghh');
/*!40000 ALTER TABLE `ClientInfo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-29 12:27:58
