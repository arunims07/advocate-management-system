import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.sql.*;

public class DeleteUser extends JFrame implements ActionListener {
    JLabel luname, lpassword, ldesignation, lrepass, ltitle;
    JTextField txtuname;
    JPasswordField txtpass, txtrepass;
    JComboBox cmb;
    JButton btnok, btncancel;
    Connection con;

    public DeleteUser() {
        super("AMS-Delete User");
        setIconImage (getToolkit().getImage ("images/IconLogo.png"));
        Container c = getContentPane();

        Color c1 = new Color(5, 1, 88);
        Color c2 = new Color(45, 40, 100);
        Color c3 = new Color(0, 0, 0);
        Font f = new Font("Black Oblique", Font.BOLD, 20);
        Font f1 = new Font("Algerian", Font.BOLD, 35);
        Font f2 = new Font("Arial", Font.BOLD, 15);
        c.setLayout(null);

        c.setBackground(new Color(245, 250, 255));
        JLabel li=new JLabel(new ImageIcon("images/IconLogo.png"));
        li.setBounds(0,0,50,50);
        c.add(li);

        ltitle = new JLabel("Delete User");
        c.add(ltitle);
        ltitle.setBounds(75, -10, 400, 75);
        ltitle.setFont(f1);
        ltitle.setForeground(new Color(3, 7, 66));

        JLabel lu = new JLabel();
        lu.setBounds(68, 43, 267, 3);
        lu.setBorder(BorderFactory.createLineBorder(Color.black, 2));
        c.add(lu);

        ldesignation = new JLabel("Designation:");
        add(ldesignation);
        ldesignation.setFont(f);
        ldesignation.setForeground(c2);
        ldesignation.setBounds(25, 60, 150, 25);

        luname = new JLabel("User Name:");
        c.add(luname);
        luname.setFont(f);
        luname.setForeground(c2);
        luname.setBounds(25, 100, 125, 25);


        cmb = new JComboBox();
        cmb.addItem("admin");
        cmb.addItem("user");
        cmb.setFont(f2);
        cmb.setForeground(c3);
        c.add(cmb);
        cmb.setBounds(225, 60, 150, 25);


        txtuname = new JTextField();
        c.add(txtuname);
        txtuname.setFont(f2);
        txtuname.setForeground(c3);
        txtuname.setBounds(225, 100, 150, 25);


        btnok = new JButton(new ImageIcon("images/Login.jpg"));
        btnok.setOpaque(false);
        c.add(btnok);
        btnok.setBounds(85, 250, 50, 50);

        btncancel = new JButton(new ImageIcon("images/Cancel.jpg"));
        btncancel.setOpaque(false);
        c.add(btncancel);
        btncancel.setBounds(255, 250, 50, 50);


        setLayout(null);
        setBounds(25, 75, 450, 350);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setResizable(false);


        btnok.addActionListener(this);
        btncancel.addActionListener(this);

        //Get DataBase Obejct and Connection Object for executing Query
        JDBCConnection jdbc = new JDBCConnection();
        con = jdbc.getConnectionObject();


    }

    //-------------------------------------------------------------------------------------------------//
    public void actionPerformed(ActionEvent ae) {

        if (ae.getSource() == btncancel) {
            setVisible(false);
            this.dispose();

        }


        b:
        if (ae.getSource() == btnok) {
            if (txtuname.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Please enter the User Name...");
                txtuname.requestFocus();
            } else {
                try {
                    Statement st = con.createStatement();
                    ResultSet rs = st.executeQuery("select UserName from login");
                    while (rs.next()) {
                        if (txtuname.getText().equals(rs.getString("UserName"))) {
                                String query = "DELETE From login where UserName = " + "\"" + txtuname.getText() + "\"";
                                st.executeUpdate(query);
                                JOptionPane.showMessageDialog(null, "User Deleted");
                                txtuname.setText("");

                            }
                        }

                }catch (Exception s) {
                    System.out.println(s);
                }

            }
        }


    }
}