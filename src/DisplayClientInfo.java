import java.awt.*;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DisplayClientInfo extends JFrame implements ActionListener {
    //JTable table ;

    TableRowSorter<DefaultTableModel> sorter;
    TableRowSorter<DefaultTableModel> sorter1;
    String[] columnNames = {"Client Name", "Today Hearing Date", "Case Nature", "Case No"};
    String[] columnNames1 = {"Client Name", "Next Hearing Date", "Case Nature", "Case No"};
    JButton caseNoButton = null;
    JButton byNameButton = null;
    JButton datewiseButton = null;
    JTextArea caseText = null;
    JTextArea NameText = null;
    JTextArea DateText = null;
    DefaultTableModel demodel,demodel1 = null;

    //Get DataBase Obejct and Connection Object for executing Query
    JDBCConnection jdbc = null;
    Connection con = null;

    //System.out.println(formatter.format(today));        //2017-06-04
    String textvalue = "";//textbox.getText();
    String clientName= "";
    String hearingDate= "";
    String searchgDate= "";
    String casenature = "";
    String caseNo = "";
    String rowvalue = "";

    public DisplayClientInfo() {
        super ("AMS - Display Client Information");
        setIconImage (getToolkit().getImage ("images/IconLogo.png"));

        Container c=getContentPane();
        JPanel byName = new JPanel();
        JPanel dateWise = new JPanel();
        JPanel caseNO = new JPanel();
        JPanel todayHearing = new JPanel();
        Color c1=new Color(5,1,88);
        Color c2=new Color(45,40,100);
        Color c3=new Color(220,0,0);
        Font f=new Font("Black Oblique",Font.BOLD,20);
        Font f1=new Font("Algerian",Font.BOLD,25);
        Font f2=new Font("Arial",Font.BOLD,15);
        c.setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        c.setBackground(new Color(245,250,255));
        GridLayout gridLayout = new GridLayout(0, 1,10,10);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        LocalDate today = LocalDate.now();

        //System.out.println(formatter.format(today));        //2017-06-04
        textvalue = formatter.format(today);//textbox.getText();



        demodel = new DefaultTableModel();
        demodel.setColumnIdentifiers(columnNames);

        demodel1 = new DefaultTableModel();
        demodel1.setColumnIdentifiers(columnNames1);

        //Get DataBase Obejct and Connection Object for executing Query
        jdbc = new JDBCConnection();
        con = jdbc.getConnectionObject();
        try {
            String sql = "select * from ClientInfo where TodayHD = " + "\"" + textvalue + "\"";
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                clientName = rs.getString("ClientName");
                hearingDate = rs.getString("TodayHD");
                casenature = rs.getString("CaseNature");
                caseNo = rs.getString("CaseNo");
                demodel.addRow(new Object[]{clientName, hearingDate, casenature, caseNo});

            }
        }
        catch(SQLException sx)
        {

        }

        try {

            //textvalue = "2020-03-28";
            String sql = "select * from ClientInfo where NextHD >  "  + "\"" + textvalue + "\"";
            System.out.println(sql.toString());
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                clientName = rs.getString("ClientName");
                hearingDate = rs.getString("NextHD");
                casenature = rs.getString("CaseNature");
                caseNo = rs.getString("CaseNo");
                demodel1.addRow(new Object[]{clientName, hearingDate, casenature, caseNo});

            }
        }
        catch(Exception sx)
        {
            System.out.println(sx.getMessage());
        }

        JTable table = new JTable(demodel);
        table.setPreferredScrollableViewportSize(new Dimension(700, 300));
        table.setFillsViewportHeight(true);
        table.setRowSorter(sorter);

        JTable table1 = new JTable(demodel1);
        table1.setPreferredScrollableViewportSize(new Dimension(700, 300));
        table1.setFillsViewportHeight(true);
        table1.setRowSorter(sorter1);

        caseNoButton = new JButton("Search");
        byNameButton = new JButton("Search");
        datewiseButton = new JButton("Date");
        caseText = new JTextArea("");
        NameText = new JTextArea("");
        DateText = new JTextArea("");

        // Set up the title for different panels
        byName.setBorder(BorderFactory.createTitledBorder("Name Wise"));
        dateWise.setBorder(BorderFactory.createTitledBorder("Date Wise"));
        caseNO.setBorder(BorderFactory.createTitledBorder("Case No"));

        // Set up the BoxLayout
        BoxLayout layout1 = new BoxLayout(byName, BoxLayout.X_AXIS);
        BoxLayout layout2 = new BoxLayout(dateWise, BoxLayout.X_AXIS);
        BoxLayout layout3 = new BoxLayout(caseNO, BoxLayout.X_AXIS);
        byName.setLayout(layout1);
        dateWise.setLayout(layout2);
        caseNO.setLayout(layout3);

        // Add the buttons into the panel with three different alignment options
        byNameButton.setAlignmentX(Component.LEFT_ALIGNMENT);
        NameText.setAlignmentX(Component.LEFT_ALIGNMENT);
        byName.add(NameText);
        byName.add(byNameButton);

        datewiseButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        DateText.setAlignmentX(Component.CENTER_ALIGNMENT);
        dateWise.add(DateText);
        dateWise.add(datewiseButton);

        caseNoButton.setAlignmentX(Component.RIGHT_ALIGNMENT);
        caseText.setAlignmentX(Component.RIGHT_ALIGNMENT);
        caseNO.add(caseText);
        caseNO.add(caseNoButton);

        gridLayout.addLayoutComponent("Today Hearing Date", table);
        gridLayout.addLayoutComponent("Next Hearing Date", table1);


        //For the purposes of this example, better to have a single
        //selection.
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        todayHearing.setLayout(gridLayout);

        c.add(byName);
        c.add(dateWise);
        c.add(caseNO);
        datewiseButton.addActionListener(this);
        caseNoButton.addActionListener(this);
        byNameButton.addActionListener(this);

        int column = 3;
        //table.getModel().getValueAt(row, column).toString();

        //both view and model.
        //When selection changes, provide user with row numbers for
        table.getSelectionModel().addListSelectionListener(
                new ListSelectionListener() {
                    public void valueChanged(ListSelectionEvent event) {
                        int viewRow = table.getSelectedRow();
                        if (viewRow < 0) {
                            //new DisplayClientFullInfo();
                        } else {
                            int modelRow =
                                    table.convertRowIndexToModel(viewRow);
                            rowvalue = table.getModel().getValueAt(viewRow, column).toString();
                            System.out.println(rowvalue);
                            new DisplayClientFullInfo(rowvalue);
                        }
                    }
                }
        );

        //When selection changes, provide user with row numbers for
        table1.getSelectionModel().addListSelectionListener(
                new ListSelectionListener() {
                    public void valueChanged(ListSelectionEvent event) {
                        int viewRow = table1.getSelectedRow();
                        if (viewRow < 0) {
                           // new DisplayClientFullInfo();
                        } else {
                            int modelRow =
                                    table1.convertRowIndexToModel(viewRow);
                            rowvalue = table1.getModel().getValueAt(viewRow, column).toString();
                            new DisplayClientFullInfo(rowvalue);

                        }
                    }
                }
        );
        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);

        //Add the scroll pane to this panel.
        c.add(scrollPane);

        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane1 = new JScrollPane(table1);

        //Add the scroll pane to this panel.
        c.add(scrollPane1);


        setVisible(true);
        setBounds(160,75,1040,750);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setResizable(false);
    }

    public void actionPerformed(ActionEvent ae) {
        Object obj = ae.getSource();

        if (obj == datewiseButton)
        {
            //JOptionPane.showMessageDialog(null,"Are you Sure , Data will not update");
            //this.setVisible(false);
            //this.dispose();
            DateText.setText(new DatePicker(this).setPickedDate());
            textvalue = DateText.getText();
            //demodel1.fireTableDataChanged();
            demodel1.setRowCount(0);
            try {
                String sql = "select * from ClientInfo where NextHD = " + "\"" + textvalue + "\"";
                PreparedStatement ps = con.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                while(rs.next()) {
                    clientName = rs.getString("ClientName");
                    hearingDate = rs.getString("TodayHD");
                    casenature = rs.getString("CaseNature");
                    caseNo = rs.getString("CaseNo");
                    searchgDate = rs.getString("NextHD");
                   // demodel.addRow(new Object[]{clientName, hearingDate, casenature, caseNo});
                    demodel1.addRow(new Object[]{clientName, searchgDate, casenature, caseNo});

                }
            }
            catch(SQLException sx)
            {

            }
        }

        if (obj == byNameButton)
        {

            demodel.setRowCount(0);
            try {
                String sql = "select * from ClientInfo where ClientName = " + "\"" + NameText.getText() + "\"";
                PreparedStatement ps = con.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                while(rs.next()) {
                    clientName = rs.getString("ClientName");
                    hearingDate = rs.getString("TodayHD");
                    casenature = rs.getString("CaseNature");
                    caseNo = rs.getString("CaseNo");
                    searchgDate = rs.getString("NextHD");
                    demodel.addRow(new Object[]{clientName, hearingDate, casenature, caseNo});
                    //demodel1.addRow(new Object[]{clientName, searchgDate, casenature, caseNo});


                }
            }
            catch(SQLException sx)
            {

            }

        }

        if (obj == caseNoButton)
        {
            demodel.setRowCount(0);

            try {
                String sql = "select * from ClientInfo where CaseNo = " + "\"" + caseText.getText() + "\"";
                PreparedStatement ps = con.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                while(rs.next()) {
                    clientName = rs.getString("ClientName");
                    hearingDate = rs.getString("TodayHD");
                    casenature = rs.getString("CaseNature");
                    caseNo = rs.getString("CaseNo");
                    searchgDate = rs.getString("NextHD");
                    demodel.addRow(new Object[]{clientName, hearingDate, casenature, caseNo});
                    //demodel1.addRow(new Object[]{clientName, searchgDate, casenature, caseNo});

                }
            }
            catch(SQLException sx)
            {

            }

        }
    }

}
