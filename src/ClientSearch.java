import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class ClientSearch extends JFrame implements ActionListener {
    JLabel  luname,ldesignation, ltitle;
    JTextField txtuname, txtMob;
    JComboBox cmb;
    JButton btnok, btncancel;
    Connection con;

    public ClientSearch() {
        super("AMS-Client Details");
        setIconImage(getToolkit().getImage("images/IconLogo.png"));
        Container c = getContentPane();

        Color c1 = new Color(5, 1, 88);
        Color c2 = new Color(45, 40, 100);
        Color c3 = new Color(0, 0, 0);
        Font f = new Font("Black Oblique", Font.BOLD, 15);
        Font f1 = new Font("Algerian", Font.BOLD, 20);
        Font f2 = new Font("Arial", Font.BOLD, 15);
        c.setLayout(null);

        c.setBackground(new Color(245, 250, 255));

        JLabel li = new JLabel(new ImageIcon("images/IconLogo.png"));
        li.setBounds(0, 0, 50, 50);
        c.add(li);

        ltitle = new JLabel("Please Fill Client Details");
        c.add(ltitle);
        ltitle.setBounds(75, -10, 400, 75);
        ltitle.setFont(f1);
        ltitle.setForeground(new Color(3, 7, 66));

        JLabel lu = new JLabel();
        lu.setBounds(68, 43, 267, 3);
        lu.setBorder(BorderFactory.createLineBorder(Color.black, 2));
        c.add(lu);

        luname = new JLabel("Client Name:");
        c.add(luname);
        luname.setFont(f);
        luname.setForeground(c2);
        luname.setBounds(25, 100, 125, 25);


        ldesignation = new JLabel("Mobile No");
        add(ldesignation);
        ldesignation.setFont(f);
        ldesignation.setForeground(c2);
        ldesignation.setBounds(25, 140, 150, 25);


        txtuname = new JTextField();
        c.add(txtuname);
        txtuname.setFont(f2);
        txtuname.setForeground(c3);
        txtuname.setBounds(225, 100, 150, 25);

        txtMob = new JTextField();
        c.add(txtMob);
        txtMob.setFont(f2);
        txtMob.setForeground(c3);
        txtMob.setBounds(225, 150, 150, 25);


        btnok = new JButton(new ImageIcon("images/Login.jpg"));
        btnok.setOpaque(false);
        c.add(btnok);
        btnok.setBounds(85, 250, 50, 50);

        btncancel = new JButton(new ImageIcon("images/Cancel.jpg"));
        btncancel.setOpaque(false);
        c.add(btncancel);
        btncancel.setBounds(255, 250, 50, 50);


        setLayout(null);
        setBounds(25, 75, 450, 350);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setResizable(false);

        //JLabel ll=new JLabel(new ImageIcon("images/t2.jpg"));
        //ll.setBounds(25,75,450,350);
        //c.add(ll);


        btnok.addActionListener(this);
        btncancel.addActionListener(this);

        //Get DataBase Obejct and Connection Object for executing Query
        JDBCConnection jdbc = new JDBCConnection();
        con = jdbc.getConnectionObject();


    }

    //-------------------------------------------------------------------------------------------------//
    public void actionPerformed(ActionEvent ae) {

        if (ae.getSource() == btncancel) {
            setVisible(false);
            this.dispose();

        }

        if (ae.getSource() == btnok) {

            new ClientUpdate(txtuname.getText(),txtMob.getText());
            setVisible(false);
            this.dispose();
        }
    }

}

