import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DisplayAllClientInfo extends JFrame implements ActionListener {

    TableRowSorter<DefaultTableModel> sorter;
    String[] columnNames = {"Client Name", "Today Hearing Date", "Case Nature", "Case No"};
    JButton datewiseButton = null;
    JTextArea DateText = null;
    DefaultTableModel demodel = null;

    //Get DataBase Obejct and Connection Object for executing Query
    JDBCConnection jdbc = null;
    Connection con = null;

    //System.out.println(formatter.format(today));        //2017-06-04
    String textvalue = "";//textbox.getText();
    String clientName= "";
    String hearingDate= "";
    String searchgDate= "";
    String casenature = "";
    String caseNo = "";
    String rowvalue = "";

    public DisplayAllClientInfo() {
        super ("AMS - Display All Client Information");
        setIconImage (getToolkit().getImage ("images/IconLogo.png"));

        Container c=getContentPane();
        Color c1=new Color(5,1,88);
        Color c2=new Color(45,40,100);
        Color c3=new Color(220,0,0);
        Font f=new Font("Black Oblique",Font.BOLD,20);
        Font f1=new Font("Algerian",Font.BOLD,25);
        Font f2=new Font("Arial",Font.BOLD,15);
        c.setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        c.setBackground(new Color(245,250,255));
        GridLayout gridLayout = new GridLayout(0, 1,10,10);


        demodel = new DefaultTableModel();
        demodel.setColumnIdentifiers(columnNames);

        //Get DataBase Obejct and Connection Object for executing Query
        jdbc = new JDBCConnection();
        con = jdbc.getConnectionObject();
        try {
            String sql = "select * from ClientInfo";
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                clientName = rs.getString("ClientName");
                hearingDate = rs.getString("TodayHD");
                casenature = rs.getString("CaseNature");
                caseNo = rs.getString("CaseNo");
                demodel.addRow(new Object[]{clientName, hearingDate, casenature, caseNo});

            }
        }
        catch(SQLException sx)
        {

        }


        JTable table = new JTable(demodel);
        table.setPreferredScrollableViewportSize(new Dimension(700, 300));
        table.setFillsViewportHeight(true);
        table.setRowSorter(sorter);

        gridLayout.addLayoutComponent("Today Hearing Date", table);


        //For the purposes of this example, better to have a single
        //selection.
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        int column = 3;

        //both view and model.
        //When selection changes, provide user with row numbers for
        table.getSelectionModel().addListSelectionListener(
                new ListSelectionListener() {
                    public void valueChanged(ListSelectionEvent event) {
                        int viewRow = table.getSelectedRow();
                        if (viewRow < 0) {
                        } else {
                            int modelRow =
                                    table.convertRowIndexToModel(viewRow);
                            rowvalue = table.getModel().getValueAt(viewRow, column).toString();
                            System.out.println(rowvalue);
                            new DisplayClientFullInfo(rowvalue);
                        }
                    }
                }
        );
        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);

        //Add the scroll pane to this panel.
        c.add(scrollPane);


        setVisible(true);
        setBounds(160,75,1040,750);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setResizable(false);
    }

    public void actionPerformed(ActionEvent ae) {
        Object obj = ae.getSource();

        if (obj == datewiseButton)
        {
            DateText.setText(new DatePicker(this).setPickedDate());
            textvalue = DateText.getText();
            try {
                String sql = "select * from ClientInfo where NextHD = " + "\"" + textvalue + "\"";
                PreparedStatement ps = con.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                while(rs.next()) {
                    clientName = rs.getString("ClientName");
                    hearingDate = rs.getString("TodayHD");
                    casenature = rs.getString("CaseNature");
                    caseNo = rs.getString("CaseNo");
                    searchgDate = rs.getString("NextHD");
                    demodel.addRow(new Object[]{clientName, hearingDate, casenature, caseNo});

                }
            }
            catch(SQLException sx)
            {

            }
        }

    }

}
