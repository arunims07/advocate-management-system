import javax.swing.*;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {

    Connection con;

    JDBCConnection() {
        try {
            // Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
            Driver driver = new com.mysql.cj.jdbc.Driver();
            DriverManager.registerDriver(driver);

            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ams_db", "root", "mysql@424");
            //JOptionPane.showMessageDialog(null, "Connection Open", "Verification", JOptionPane.PLAIN_MESSAGE);
        }
        //catch (ClassNotFoundException cnf)  {
        //	JOptionPane.showMessageDialog (null, "Driver not Loaded...");
        //	System.exit (0);
        //}
        catch (
                SQLException sqlex) {
            JOptionPane.showMessageDialog(null, "Unable to Connect to Database...");
            System.exit(0);
        }
    }

    public Connection getConnectionObject()
    {
        return  con;
    }
}
