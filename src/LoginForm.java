import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.sql.*;


public class LoginForm extends JFrame implements ActionListener
{
	JLabel luname,lpassword,ldegination,ltitle,l,copyRight;
	JTextField txtuname;
	JPasswordField txtpass;
	JComboBox cmb;
	JButton btnok,btncancel;
	Connection con;
	
	public LoginForm()
	{
		super ("AMS - Login");
		setIconImage (getToolkit().getImage ("images/IconLogo.png"));
		Container c=getContentPane();
		Color c1=new Color(5,8,88);
		Color c2=new Color(45,40,100);
		Color c3=new Color(0,0,0);
		Font f=new Font("Black Oblique",Font.BOLD,20);
		Font f1=new Font("Algerian",Font.BOLD,35);
		Font f2=new Font("Arial",Font.BOLD,15);
		Font f3=new Font("Arial",Font.ITALIC,15);
		c.setLayout(null);
		

		ltitle=new JLabel("User Login");
		c.add(ltitle);	
		ltitle.setBounds(200,5,400,75);
		ltitle.setFont(f1);
		ltitle.setForeground(new Color(15,5,50));

		JLabel lu=new JLabel();
		lu.setBounds(170,57,250,3);
		lu.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		c.add(lu);

		ldegination=new JLabel("Designation:");
		add(ldegination);
		ldegination.setFont(f);
		ldegination.setForeground(c2);
		ldegination.setBounds(70,150,150,30);

		luname=new JLabel("User Name:");
		c.add(luname);
		luname.setFont(f);
		luname.setForeground(c2);
		luname.setBounds(70,200,125,30);

		lpassword=new JLabel("Password:");
		c.add(lpassword);
		lpassword.setFont(f);
		lpassword.setForeground(c2);
		lpassword.setBounds(70,250,125,30);

		cmb=new JComboBox();
		cmb.addItem("admin");
		cmb.addItem("user");
		c.add(cmb);
		cmb.setFont(f2);
		cmb.setForeground(c3);
		cmb.setBounds(220,150,150,30);


		txtuname=new JTextField();
		c.add(txtuname);
		txtuname.setBounds(220,200,150,30);
		txtuname.setFont(f2);
		txtuname.setForeground(c3);

		txtpass=new JPasswordField();
		c.add(txtpass);
		txtpass.setBounds(220,250,150,30);
		txtpass.setFont(f2);
		txtpass.setForeground(c3);		
		btnok=new JButton(new ImageIcon("images/Login.jpg"));
		btnok.setOpaque(false);
		c.add(btnok);
		btnok.setBounds(60,350,50,50);

		btncancel=new JButton(new ImageIcon("images/Cancel.jpg"));
		btncancel.setOpaque(false);
		c.add(btncancel);
		btncancel.setBounds(170,350,50,50);

		copyRight=new JLabel("<html>KAAM Software Pvt Ltd<br/>Copyright @2020 , All Rights Reserved</html>", SwingConstants.CENTER);
		c.add(copyRight);
		copyRight.setBounds(210,350,300,75);
		copyRight.setFont(f3);
		copyRight.setForeground(new Color(15,5,50));

		l=new JLabel(new ImageIcon("images/lg.jpg"));
		l.setBounds(0,-100,500,600);
		add(l);
		
		setLayout(null);
		setBounds(500,0,500,435);
		setVisible(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);		

		//c.setBackground(new Color(255,255,251));
		btnok.addActionListener(this);
		btncancel.addActionListener(this);

        //Get DataBase Obejct and Connection Object for executing Query
		JDBCConnection jdbc = new JDBCConnection();
		con = jdbc.getConnectionObject();

		
		
	}
	public void actionPerformed(ActionEvent ae)
		{
			
			if(ae.getSource()==btncancel)
			{
				setVisible(false);
				this.dispose();
	
			}


		b:	if(ae.getSource()==btnok) 
			{
				if(txtuname.getText().equals(""))
				         JOptionPane.showMessageDialog (null, "Please enter the User Name...");
				else if(txtpass.getText().equals(""))
				        JOptionPane.showMessageDialog(null,"Plesae enter password");
				
				else
				{
					try
					{
					Statement	cs=con.createStatement();
					
					ResultSet rs=cs.executeQuery("select * from login");

					String p=String.valueOf(txtpass.getPassword());
					while(rs.next())
					{

						if(txtuname.getText().equals(rs.getString(2)) && p.equals(rs.getString(3)) && cmb.getSelectedItem().equals(rs.getString(4)) )
						{
							JOptionPane.showMessageDialog(null," Welcome ! You are a valid user.","verification",JOptionPane.PLAIN_MESSAGE);


							if(cmb.getSelectedItem().equals("user"))
							{
								new ClientWindow();

							}
							if(cmb.getSelectedItem().equals("admin"))
							{
								new AdminWindow();

							}

							this.setVisible(false);
							this.dispose();
							break b;
						}
						if(txtuname.getText().equals(rs.getString(2)) && p.equals(rs.getString(3)) && rs.getString(4).equals("Master"))
						{
							JOptionPane.showMessageDialog(null," Welcome ! You are a Master.","verification",JOptionPane.PLAIN_MESSAGE);
							new MasterWindow();

							this.setVisible(false);
							this.dispose();
							break b;
						}


					}
					JOptionPane.showMessageDialog(null,"Invalid User","unverification",JOptionPane.PLAIN_MESSAGE);	
					txtpass.setText ("");
					txtuname.requestFocus ();
					
					}
	
					catch(Exception s)
					{
						System.out.println(s);
					}
				}
			}
			

		}

	public static void main(String [] args)
	{
		new LoginForm();
	}
}