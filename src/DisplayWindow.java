import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DisplayWindow extends JFrame implements ActionListener {


    JLabel label3,ltitle;
    JButton newClientInfo;
    JButton exitClientInfo;


    public DisplayWindow()
    {
        //--------------------------------------------------------------------------------------------------------------------------------------//
        super ("AMS - Display Client Window");
        setIconImage (getToolkit().getImage ("images/IconLogo.png"));
        Container c=getContentPane();
        Font f1=new Font("Algerian",Font.BOLD,15);
        c.setLayout(null);
        ltitle=new JLabel("Display Client Information");
        c.add(ltitle);
        ltitle.setBounds(150,5,400,75);
        ltitle.setFont(f1);
        ltitle.setForeground(new Color(15,5,50));


        //----------------------Background Image--------------------------------------//

        newClientInfo = new JButton("According to Hearing Date");

        exitClientInfo = new JButton("Show All Client Info ");

        c.add(newClientInfo);
        newClientInfo.setBounds(100,150,200,90);
        newClientInfo.setForeground(Color.BLUE);
        newClientInfo.addActionListener(this);

        c.add(exitClientInfo);
        exitClientInfo.setBounds(100,250,200,90);
        exitClientInfo.setForeground(Color.GREEN);
        exitClientInfo.addActionListener(this);



        label3=new JLabel(new ImageIcon("images/lg_1.jpeg"));
        label3.setBounds(0,-60,500,600);
        add(label3);

        setLayout(null);
        setBounds(400,0,400,400);
        setForeground(new Color(51,153,255));
        setBackground(new Color(51,153,255));
        setVisible(true);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    }

    public void actionPerformed(ActionEvent ae) {
        Object obj = ae.getSource();

        if(obj==newClientInfo)
        {

          // new ClientInfo();
            new DisplayClientInfo();
           // this.setVisible(false);
            //this.dispose();
        }

        if(obj==exitClientInfo)
        {

            // new ClientInfo();
            new DisplayAllClientInfo();
            //this.setVisible(false);
            //this.dispose();
        }
    }
}
