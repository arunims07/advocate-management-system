-- MySQL dump 10.13  Distrib 8.0.19, for macos10.15 (x86_64)
--
-- Host: localhost    Database: AMS_DB
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Court`
--

DROP TABLE IF EXISTS `Court`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Court` (
  `CourtID` int NOT NULL AUTO_INCREMENT,
  `CourtName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CourtID`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Court`
--

LOCK TABLES `Court` WRITE;
/*!40000 ALTER TABLE `Court` DISABLE KEYS */;
INSERT INTO `Court` VALUES (1,'Agra'),(2,'Aligarh'),(3,'Allahabad'),(4,'Ambedkar Nagar at Akbarpur'),(5,'Amethi'),(6,'Amroha'),(7,'Auraiya'),(8,'Azamgarh'),(9,'Baghpat'),(10,'Bahraich'),(11,'Ballia'),(12,'Balrampur'),(13,'Banda'),(14,'Barabanki'),(15,'Bareilly'),(16,'Basti'),(17,'Bhadohi at Gyanpur'),(18,'Bijnor'),(19,'Budaun'),(20,'Bulandshahar'),(21,'Chandauli'),(22,'Chitrakoot'),(23,'Deoria'),(24,'Etah'),(25,'Etawah'),(26,'Faizabad'),(27,'Farukkhabad'),(28,'Fatehpur'),(29,'Firozabad'),(30,'Gautambuddha Nagar'),(31,'Ghaziabad'),(32,'Ghazipur'),(33,'Gonda'),(34,'Gorakhpur'),(35,'Hamirpur'),(36,'Hapur'),(37,'Hardoi'),(38,'Hathras'),(39,'Jalaun at Orai'),(40,'Jaunpur'),(41,'Jhansi'),(42,'Kannauj'),(43,'Kanpur Nagar'),(44,'Kasganj'),(45,'Kaushambi'),(46,'Kushi Nagar at Padrauna'),(47,'Lakhimpur Kheri'),(48,'Lalitpur'),(49,'Lucknow'),(50,'Maharajganj'),(51,'Mahoba'),(52,'Mainpuri'),(53,'Mathura'),(54,'Mau'),(55,'Meerut'),(56,'Mirzapur'),(57,'Moradabad'),(58,'Muzaffarnagar'),(59,'Pilibhit'),(60,'Pratapgarh'),(61,'Raebareli'),(62,'Ramabai Nagar(Kanpur Dehat)'),(63,'Rampur'),(64,'Saharanpur'),(65,'Sambhal at Chandausi'),(66,'Sant Kabir Nagar'),(67,'Shahjahanpur'),(68,'Shamli'),(69,'Shravasti'),(70,'Siddharth Nagar'),(71,'Sitapur'),(72,'Sonbhadra'),(73,'Sultanpur'),(74,'Unnao'),(75,'Varanasi');
/*!40000 ALTER TABLE `Court` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-29 12:27:58
