import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdminWindow extends JFrame implements ActionListener {


    JLabel label3,ltitle,copyRight;
    JButton newClientInfo;
    JButton exitClientInfo;
    JButton updateClientInfo;
    JButton miscButton;
    JButton createUser;
    JButton deleteUser;

    public AdminWindow()
    {
        //--------------------------------------------------------------------------------------------------------------------------------------//
        super ("AMS - Admin Window");
        setIconImage (getToolkit().getImage ("images/IconLogo.png"));
        Container c=getContentPane();
        Font f1=new Font("Algerian",Font.BOLD,35);
        Font f3=new Font("Arial",Font.ITALIC,12);
        c.setLayout(null);
        c.setForeground(new Color(51,153,255));
        ltitle=new JLabel("Admin Window");
        c.add(ltitle);
        ltitle.setBounds(150,5,400,75);
        ltitle.setFont(f1);
        ltitle.setForeground(new Color(15,5,50));


        //----------------------Background Image--------------------------------------//

        newClientInfo = new JButton("Add New Client Info");

        exitClientInfo = new JButton("Show All Client Info ");

        updateClientInfo = new JButton("Update Client Info ");
        deleteUser = new JButton("Delete User ");
        createUser = new JButton("Create New User");
        miscButton = new JButton("Miscellaneous");


        newClientInfo.setBounds(80,150,150,70);
        newClientInfo.setForeground(Color.BLACK);
        newClientInfo.setBackground(new Color(250,128,114));
        c.add(newClientInfo);
        newClientInfo.addActionListener(this);

        c.add(exitClientInfo);
        exitClientInfo.setBounds(330,150,150,70);
        //updateTableData.setOpaque(true);
        exitClientInfo.setForeground(Color.BLACK);
        exitClientInfo.setBackground(new Color(250,128,114));
        exitClientInfo.addActionListener(this);

        c.add(updateClientInfo);
        updateClientInfo.setBounds(80,250,150,70);
        updateClientInfo.setForeground(Color.BLACK);
        updateClientInfo.setBackground(new Color(250,128,114));
        updateClientInfo.addActionListener(this);

        c.add(createUser);
        createUser.setBounds(330,250,150,70);
        createUser.setForeground(Color.BLACK);
        createUser.setBackground(new Color(250,128,114));
        createUser.addActionListener(this);

        c.add(deleteUser);
        deleteUser.setBounds(80,350,150,70);
        deleteUser.setForeground(Color.BLACK);
        deleteUser.setBackground(new Color(250,128,114));
        deleteUser.addActionListener(this);

        c.add(miscButton);
        miscButton.setBounds(330,350,150,70);
        miscButton.setForeground(Color.BLACK);
        miscButton.setBackground(new Color(250,128,114));
        miscButton.addActionListener(this);

        copyRight=new JLabel("<html>KAAM Software Pvt Ltd<br/>Copyright @2020 , All Rights Reserved</html>", SwingConstants.CENTER);
        c.add(copyRight);
        copyRight.setBounds(200,430,300,50);
        copyRight.setFont(f3);
        copyRight.setForeground(new Color(15,5,50));



        label3=new JLabel(new ImageIcon("images/lg_1.jpeg"));
        label3.setBounds(0,-60,500,600);
        add(label3);

        setLayout(null);
        setBounds(500,0,500,500);
        setForeground(new Color(51,153,255));
        setBackground(new Color(51,153,255));
        setVisible(true);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

    public void actionPerformed(ActionEvent ae) {
        Object obj = ae.getSource();

        if(obj==newClientInfo)
        {
            new ClientInfo();

        }
        if(obj==exitClientInfo)
        {
            //new DisplayClientInfo();
            new DisplayWindow();

        }
        if(obj==updateClientInfo)
        {

            //new ClientUpdate();
            new ClientSearch();
        }
        if(obj==createUser)
        {
            new CreateUser();

        }
        if(obj==deleteUser)
        {
            new DeleteUser();

        }

        if(obj==miscButton)
        {
            JOptionPane.showMessageDialog(null,"Coming Soon");
            //new DeleteUser();

        }
    }
}
