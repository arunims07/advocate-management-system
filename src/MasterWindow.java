import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MasterWindow extends JFrame implements ActionListener {


    JLabel label1,label3,ltitle,copyRight;
    Icon icon1;
    JButton allUser;
    JButton updateTableData;
    JButton updateStateInfo;
    JButton miscButton;

    public MasterWindow()
    {
        //--------------------------------------------------------------------------------------------------------------------------------------//
        super ("AMS - Master Window");
        setIconImage (getToolkit().getImage ("images/IconLogo.png"));
        Container c=getContentPane();
        Font f1=new Font("Algerian",Font.BOLD,20);
        Font f3=new Font("Arial",Font.ITALIC,12);
        c.setLayout(null);
        ltitle=new JLabel("Master Window");
        c.add(ltitle);
        ltitle.setBounds(150,5,400,75);
        ltitle.setFont(f1);
        ltitle.setForeground(new Color(15,5,50));


        //----------------------Background Image--------------------------------------//

        allUser = new JButton("Show All User");

        updateTableData = new JButton("Update Case Nature");

        updateStateInfo = new JButton("Update State");
        miscButton = new JButton("Miscellaneous");

        c.add(allUser);
        allUser.setBounds(80,150,150,90);
        allUser.setForeground(Color.BLACK);
        allUser.setBackground(new Color(250,128,114));
        allUser.addActionListener(this);

        c.add(updateTableData);
        updateTableData.setBounds(300,150,150,90);
        updateTableData.setForeground(Color.BLACK);
        updateTableData.setBackground(new Color(250,128,114));
        updateTableData.addActionListener(this);

        c.add(updateStateInfo);
        updateStateInfo.setBounds(80,250,150,90);
        updateStateInfo.setForeground(Color.BLACK);
        updateStateInfo.setBackground(new Color(250,128,114));
        updateStateInfo.addActionListener(this);

        c.add(miscButton);
        miscButton.setBounds(300,250,150,90);
        miscButton.setForeground(Color.BLACK);
        miscButton.setBackground(new Color(250,128,114));
        miscButton.addActionListener(this);

        copyRight=new JLabel("<html>KAAM Software Pvt Ltd<br/>Copyright @2020 , All Rights Reserved</html>", SwingConstants.CENTER);
        c.add(copyRight);
        copyRight.setBounds(200,430,300,50);
        copyRight.setFont(f3);
        copyRight.setForeground(new Color(15,5,50));



        label3=new JLabel(new ImageIcon("images/lg_1.jpeg"));
        label3.setBounds(0,-60,500,600);
        add(label3);

        setLayout(null);
        setBounds(500,0,500,500);
        setForeground(new Color(51,153,255));
        setBackground(new Color(51,153,255));
        setVisible(true);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    }

    public void actionPerformed(ActionEvent ae) {
        Object obj = ae.getSource();

        if(obj== allUser)
        {

          new ShowAllUser();
        }

        if(obj== updateTableData)
        {

           new AddCaseNature();
        }

        if(obj==updateStateInfo)
        {

           new AddState();
        }

        if(obj==miscButton)
        {

            JOptionPane.showMessageDialog(null,"Coming Soon");
        }
    }
}
